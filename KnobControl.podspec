

Pod::Spec.new do |s|



  s.name         = "KnobControl"
  s.version      = "1.0.0"
  s.summary      = "A knob control like the UISlider, but in a circular form."
  s.description  = "The knob control is a completely customizable widget that can be used in any iOS app. It also plays a little victory fanfare."
  s.homepage     = "http://creoit.com"


  s.license      = "MIT"


  s.author             = { "akhilesh-creo" => "akhilesh@creoit.com" }


  s.platform     = :ios, "12.0"


  s.source       = { :git => 'https://akhilesh-creo@bitbucket.org/akhilesh-creo/knobcontrol.git', :tag => '1.0.0' }

  s.source_files = "KnobControl"
  s.exclude_files = "Classes/Exclude"
  s.swift_version = "4.2" 


end
